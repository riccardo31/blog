---
template: blog_list.html
title: All the articles
description: > 
    Welcome on R2Devops blog. Here we talk about CI/CD, DevOps... all subjects related to R2Devops! 
---

# Welcome on R2Devops blog

![R2Devops Blog](./img/R2_book.png){ width="20%"}

Find below the latest articles 👇🏻

---

## ^^Latest Posts^^


<!-- Plausible -->
<script defer data-domain="blog.r2devops.io" src="https://plausible.io/js/plausible.js"></script>
<!-- End Plausible -->